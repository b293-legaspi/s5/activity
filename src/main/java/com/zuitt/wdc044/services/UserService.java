package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UserService {
    void createUser(User user);

    Optional<User> findByUsername(String username);

    Object getUser(String stringToken);

    Object myPosts (String stringToken);

}
