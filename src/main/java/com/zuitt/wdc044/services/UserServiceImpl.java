package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import com.zuitt.wdc044.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    PostRepository postRepository;


    public void createUser(User user) {
      // saves the user in our User table
      userRepository.save(user);

  }

  public Optional<User> findByUsername(String username) {
      return Optional.ofNullable(userRepository.findByUsername(username));
  }

    public Object getUser(String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        user.setPassword("");
        return user;
    }

    public Object myPosts(String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        return user.getPosts();
    }




}
